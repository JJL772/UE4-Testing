#pragma once

#include "Weather.h"
#include "Runtime/Engine/Classes/GameFramework/Volume.h"
#include "WeatherVolume.generated.h"

UCLASS()
class AWeatherVolume : public AVolume
{
	GENERATED_BODY()

public:
	TArray<AActor*> ExposedObjects;

	//UPROPERTY(EditAnywhere, meta=(ToolTip="Samples per meter. Totale samples = (x*y) * SampleDensity")) int SampleDensity;

	AWeatherVolume();

	~AWeatherVolume();

	void FindExposedObjects();
};