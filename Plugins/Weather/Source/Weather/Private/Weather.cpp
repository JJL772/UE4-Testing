// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Weather.h"

#define LOCTEXT_NAMESPACE "FWeatherModule"

void FWeatherModule::StartupModule()
{

}

void FWeatherModule::ShutdownModule()
{

}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FWeatherModule, Weather)