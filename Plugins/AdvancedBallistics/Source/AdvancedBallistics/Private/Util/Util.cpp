//===========================================================//
// Name: Util.cpp
// Purpose:
// Bugs: None.
// Primary Authors: Jeremy
// Secondary Authors: None.
// Date Of Creation: <6/10/2018> <5:06 PM>
//===========================================================//
#include "Util.h"

BALLISTICS_SOURCE

#define B BALLISTICS_NAMESPACE

static void B::Log(FString msg)
{
	UE_LOG(BallisticsLog, Log, TEXT(LOG_ID), *msg);
}

static void B::Warn(FString msg)
{
	UE_LOG(BallisticsWarn, Warning, TEXT(LOG_ID), *msg);
}

static void B::Error(FString msg)
{
	UE_LOG(BallisticsError, Error, TEXT(LOG_ID), *msg);
}

static void B::LogCL(FString msg)
{
	UE_LOG(BallisticsOpenCL, Log, TEXT(LOG_ID_CL), *msg);
}

static void B::WarnCL(FString msg)
{
	UE_LOG(BallisticsOpenCLWarn, Warning, TEXT(LOG_ID_CL), *msg);
}

static void B::ErrorCL(FString msg)
{
	UE_LOG(BallisticsOpenCLError, Error, TEXT(LOG_ID_CL), *msg);
}

#undef B