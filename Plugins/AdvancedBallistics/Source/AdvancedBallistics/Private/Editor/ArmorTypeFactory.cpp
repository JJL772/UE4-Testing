//===========================================================//
// Name: ArmorTypeFactory.cpp
// Purpose:
// Bugs: None.
// Primary Authors: Jeremy
// Secondary Authors: None.
// Date Of Creation: <6/10/2018> <11:39 PM>
//===========================================================//

#include "ArmorTypeFactory.h"

UArmorTypeFactory::UArmorTypeFactory(const FObjectInitializer& obj) :
	Super(obj)
{
	SupportedClass = UArmorType::StaticClass();

	bCreateNew = true;
}

uint32 UArmorTypeFactory::GetMenuCategories() const
{
	return EAssetTypeCategories::Gameplay;
}

UObject * UArmorTypeFactory::FactoryCreateNew(UClass * inClass, UObject * inParent, FName inName, EObjectFlags flags, UObject * context, FFeedbackContext * Warn)
{
	UArmorType* armorType = NewObject<UArmorType>(inParent, inClass, inName, flags);
	return armorType;
}
