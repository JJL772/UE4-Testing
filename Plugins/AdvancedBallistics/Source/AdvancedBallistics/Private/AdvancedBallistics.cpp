//===========================================================//
// Name: AdvancedBallistics.cpp
// Purpose:
// Bugs: None.
// Primary Authors: Jeremy
// Secondary Authors: None.
// Date Of Creation: <6/10/2018> <4:32 PM>
//===========================================================//
#include "AdvancedBallistics.h"
#include "Developer/AssetTools/Public/AssetToolsModule.h"


#define LOCTEXT_NAMESPACE "FAdvancedBallisticsModule"

/*
DECLARE_LOG_CATEGORY_EXTERN(BallisticsLog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsWarn, Warning, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsError, Error, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsOpenCL, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsOpenCLWarn, Warning, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsOpenCLError, Error, All);
*/

DEFINE_LOG_CATEGORY(BallisticsLog);
DEFINE_LOG_CATEGORY(BallisticsWarn);
DEFINE_LOG_CATEGORY(BallisticsError);
DEFINE_LOG_CATEGORY(BallisticsOpenCL);
DEFINE_LOG_CATEGORY(BallisticsOpenCLWarn);
DEFINE_LOG_CATEGORY(BallisticsOpenCLError);

//Keep includes AFTER log category definitions
#include "AdvancedBallistics/Public/Util/Util.h"


BALLISTICS_SOURCE

/*

Called on module startup

*/
void FAdvancedBallisticsModule::StartupModule()
{
	/*
	 * Set singleton!
	 */
	//pSingleton = this;

	Log("Starting up...");
	//FModuleManager manager = FModuleManager::Get();
	//pAssetToolsModule = (FAssetToolsModule*)manager.LoadModule("AssetTools");
	//AssetTools = pAssetToolsModule->Get();
	//AssetTools.RegisterAdvancedAssetCategory("Ballistics", FText::FromString("Ballistics"));
}

/*

Called on module shutdown

*/
void FAdvancedBallisticsModule::ShutdownModule()
{

}

#undef LOCTEXT_NAMESPACE
	


IMPLEMENT_MODULE(FAdvancedBallisticsModule, AdvancedBallistics)