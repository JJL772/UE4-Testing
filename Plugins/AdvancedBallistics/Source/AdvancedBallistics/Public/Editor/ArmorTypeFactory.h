//===========================================================//
// Name: ArmorTypeFactory.h
// Purpose:
// Bugs: None.
// Primary Authors: Jeremy
// Secondary Authors: None.
// Date Of Creation: <6/10/2018> <11:39 PM>
//===========================================================//
#pragma once

#include "AdvancedBallistics.h"
#include "Object.h"
#include "AssetTypeCategories.h"
#include "Editor/UnrealEd/Classes/Factories/Factory.h"
#include "ArmorType.h"
#include "ArmorTypeFactory.generated.h"



UCLASS()
class BALLISTICS_API UArmorTypeFactory : public UFactory
{
	GENERATED_BODY()

public:
	UArmorTypeFactory(const FObjectInitializer& objInit);

	virtual uint32 GetMenuCategories() const override;

	virtual UObject* FactoryCreateNew(UClass* inClass, UObject* inParent, FName inName, EObjectFlags flags, UObject* context, FFeedbackContext* Warn) override;
};