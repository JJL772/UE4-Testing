//===========================================================//
// Name: ArmorType.h
// Purpose:
// Bugs: None.
// Primary Authors: Jeremy
// Secondary Authors: None.
// Date Of Creation: <6/10/2018> <6:15 PM>
//===========================================================//
#pragma once

#include "AdvancedBallistics.h"
#include "Object.h"
#include "ArmorType.generated.h"

UCLASS()
class BALLISTICS_API UArmorType : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere) float Density;
	UPROPERTY(EditAnywhere) float Hardness;
};