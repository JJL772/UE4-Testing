//===========================================================//
// Name: AdvancedBallistics.h
// Purpose:
// Bugs: None.
// Primary Authors: Jeremy
// Secondary Authors: None.
// Date Of Creation: <6/10/2018> <4:32 PM>
//===========================================================//
#pragma once

#include "CoreMinimal.h"
#include "ModuleManager.h"
#include "Developer/AssetTools/Public/AssetToolsModule.h"

/*
Basic plugin properties
*/
#define VERSION "0.1.1"
#define LOG_ID "[Ballistics]"
#define LOG_ID_CL "[Ballistics][OpenCL]"

//Log categories
DECLARE_LOG_CATEGORY_EXTERN(BallisticsLog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsWarn, Warning, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsError, Error, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsOpenCL, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsOpenCLWarn, Warning, All);
DECLARE_LOG_CATEGORY_EXTERN(BallisticsOpenCLError, Error, All);

//Calling conventions and stuff
#define BALLISTICS_NAMESPACE Ballistics
#define BALLISTICS_HEADER_START namespace BALLISTICS_NAMESPACE {
#define BALLISTICS_HEADER_END }
#define BALLISTICS_SOURCE using namespace BALLISTICS_NAMESPACE;
#define BALLISTICS_API

class FAdvancedBallisticsModule : public IModuleInterface
{
private:
	static FAdvancedBallisticsModule * pSingleton;
public:
	/*
	 * External modules to be loaded
	 */
	//IAssetTools & AssetTools;
	//FAssetToolsModule* pAssetToolsModule;

public:

	virtual void StartupModule() override;

	virtual void ShutdownModule() override;

	//static inline FAdvancedBallisticsModule& Get() { return *pSingleton; };
};

