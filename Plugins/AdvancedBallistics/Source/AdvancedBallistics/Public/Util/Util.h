//===========================================================//
// Name: Util.h
// Purpose:
// Bugs: None.
// Primary Authors: Jeremy
// Secondary Authors: None.
// Date Of Creation: <6/10/2018> <5:06 PM>
//===========================================================//
#pragma once

#include "AdvancedBallistics.h"

BALLISTICS_HEADER_START

static void Log(FString msg);

static void Warn(FString msg);

static void Error(FString msg);

static void LogCL(FString msg);

static void WarnCL(FString msg);

static void ErrorCL(FString msg);

BALLISTICS_HEADER_END