// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class AdvancedBallistics : ModuleRules
{
	public AdvancedBallistics(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		//stuff
		//PublicDefinitions.Add("USE_OPENCL=1");

		PublicIncludePaths.AddRange(
			new string[] {
				"Ballistics/Public",
				// ... add public include paths required here ...
				Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/", "OpenCL", "Include")),
			}
		);


		PrivateIncludePaths.AddRange(
			new string[] {
				"Ballistics/Private",
				// ... add other private include paths required here ...
			}
		);


		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"AssetTools",
			}
		);


		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"UnrealEd",
				// ... add private dependencies that you statically link with here ...	
			}
		);


		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
		);

		string NvidiaWin64OpenCL = Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/OpenCL/Lib/Nvidia/Win64/OpenCL.lib"));
		string NvidiaWin32OpenCL = Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/OpenCL/Lib/Nvidia/Win32/OpenCL.lib"));
		string IntelWin64OpenCL = Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/OpenCL/Lib/Intel/Win64/OpenCL.lib"));
		string IntelWin32OpenCL = Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/OpenCL/Lib/Intel/Win32/OpenCL.lib"));
		string AMDWin64OpenCL = Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/OpenCL/Lib/AMD/Win64/OpenCL.lib"));
		string AMDWin32OpenCL = Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/OpenCL/Lib/AMD/Win64/OpenCL.lib"));

		if(Target.Platform == UnrealTargetPlatform.Win64)
		{
			PublicAdditionalLibraries.Add(NvidiaWin64OpenCL);

			//PublicAdditionalLibraries.Add(IntelWin64OpenCL);

			//PublicAdditionalLibraries.Add(AMDWin64OpenCL);
		}
		else if(Target.Platform == UnrealTargetPlatform.Win32)
		{
			PublicAdditionalLibraries.Add(NvidiaWin32OpenCL);

			//PublicAdditionalLibraries.Add(IntelWin32OpenCL);

			//PublicAdditionalLibraries.Add(AMDWin32OpenCL);
		}
	}
}
