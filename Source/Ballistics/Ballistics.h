// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Core/Public/Modules/ModuleInterface.h"

class FBallisticsGameplayModule : IModuleInterface
{
public:
	virtual void StartupModule();

	virtual void ShutdownModule();
};