// Fill out your copyright notice in the Description page of Project Settings.

#include "BallisticsGameModeBase.h"
#include "GamePawn.h"

ABallisticsGameModeBase::ABallisticsGameModeBase(const FObjectInitializer& obi) :
	Super(obi)
{
	this->DefaultPawnClass = AGamePawn::StaticClass();
}

