// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BallisticsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BALLISTICS_API ABallisticsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
		ABallisticsGameModeBase(const FObjectInitializer& obi);

};
