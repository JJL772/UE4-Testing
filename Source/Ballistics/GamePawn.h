#pragma once



#include "Ballistics.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPawn.h"
#include "GamePawn.generated.h"

UCLASS()
class AGamePawn : public ADefaultPawn
{
	GENERATED_BODY()

public:
	AGamePawn(const FObjectInitializer& objInit);

	virtual void BeginPlay();
};