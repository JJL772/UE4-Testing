#pragma once

#include "Ballistics.h"
#include "Runtime/Engine/Classes/GameFramework/Info.h"
#include "Runtime/Landscape/Classes/Landscape.h"
#include "TestActor.generated.h"

UCLASS()
class ATestActor : public AInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere) ALandscape* Landscape;

	virtual void BeginPlay();
};