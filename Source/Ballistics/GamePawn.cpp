#include "GamePawn.h"

#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/SlateCore/Public/SlateCore.h"
#include "SlateOptMacros.h"
#include "Slate.h"

#define LOCTEXT_NAMESPACE "TEST"

AGamePawn::AGamePawn(const FObjectInitializer& obj) :
	Super(obj)
{

}

void AGamePawn::BeginPlay()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "No.");
	//GEngine->GameViewport->AddViewportWidgetContent();
}

#undef LOCTEXT_NAMESPACE