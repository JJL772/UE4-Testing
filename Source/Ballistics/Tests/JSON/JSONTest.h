/*
 * 
 * JSON Serialization/Deserialization tests
 *
 * By: Jeremy Lorelli 
 * DOC: 6-14-2018 2:23AM
 *
 *
 *
 */

#pragma once

#include "Ballistics.h"

//JSON module includes
#include "Runtime/Json/Public/Serialization/JsonSerializer.h"
#include "Runtime/Json/Public/Serialization/JsonReader.h"

//Other includes
#include "Runtime/Core/Public/Misc/Paths.h"
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformFile.h"
#include "Runtime/Core/Public/HAL/PlatformFilemanager.h"
#include "Runtime/Core/Public/Containers/UnrealString.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"


class JSONSerializationTest final
{
private:
public:
	static void Run();
};

class JSONDeserializationTest final
{
private:
public:
	static void Run();
};