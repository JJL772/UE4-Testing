/*
*
* JSON Serialization/Deserialization tests
*
* By: Jeremy Lorelli
* DOC: 6-14-2018 2:23AM
*
*
*
*/

#include "JSONTest.h"

void JSONSerializationTest::Run()
{
	//Construct our JSON object and add some random stuff
	FJsonObject JsonObject;

	JsonObject.SetBoolField("works", true);
	JsonObject.SetStringField("message", "Hello!");

	FString ContentPath = FPaths::GameContentDir();

	IPlatformFile& Fs = FPlatformFileManager::Get().GetPlatformFile();

	IFileHandle* hFile = Fs.OpenWrite(*FPaths::Combine(ContentPath, "/Tests/Test1.json"));

	if (hFile == nullptr)
		return;

	TSharedRef<TJsonWriter<TCHAR>> Writer;

	FString Output;

	Writer = TJsonWriterFactory<TCHAR>::Create(&Output);

	TSharedRef<FJsonObject> ObjRef(&JsonObject);

	FJsonSerializer::Serialize<TCHAR>(ObjRef, Writer, true);
}

void JSONDeserializationTest::Run()
{
	//The JSON reader
	TSharedRef<TJsonReader<TCHAR>> JsonReader;

	//Get content directory
	FString ContentPath = FPaths::GameContentDir();

	//Setup IPlatformFile stuff
	IPlatformFile& Fs = FPlatformFileManager::Get().GetPlatformFile();

	//Open file read handle
	IFileHandle* hFile = Fs.OpenRead(*FPaths::Combine(ContentPath, "/Tests/Test.json"));

	//Create the buffer
	uint8* buffer = (uint8*)malloc(sizeof(uint8) * hFile->Size());

	//Read the stuff
	hFile->Read(buffer, hFile->Size());

	//Convert buffer to FString
	FString str = BytesToString(buffer, hFile->Size());

	//Use the factory object to do the magic
	JsonReader = TJsonReaderFactory<TCHAR>::Create(*str);

	//Our returned object will be stored here
	TSharedPtr<FJsonObject> OutObject;
	
	//Actually deserialize the object
	FJsonSerializer::Deserialize<TCHAR>(JsonReader, OutObject);

	//Default string
	FString FieldStr = "Field not found";

	//Try to get the field
	OutObject->TryGetStringField("message", FieldStr);

	//Print message to screen
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FieldStr);
}
