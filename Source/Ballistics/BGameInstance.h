#pragma once

#include "Ballistics.h"
#include "Runtime/Engine/Classes/Engine/GameInstance.h"
#include "BGameInstance.generated.h"

UCLASS()
class UBGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Shutdown();
	virtual void Init();
	virtual void OnStart();

};